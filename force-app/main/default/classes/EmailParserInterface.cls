public with sharing class EmailParserInterface {

    public Boolean  success        {get; set;}
    public String   message        {get; set;}

    public String   fromAddress    {get; set;}

}