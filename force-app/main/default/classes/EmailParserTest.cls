@isTest
public with sharing class EmailParserTest {

    static testmethod void TestParser01(){

        String bodyText = '\r\n';
        bodyText += '___________\r\n';
        bodyText += 'KDZ aanvraag \r\n';
        bodyText += 'From: Ronald van Duijkeren <rduijkeren@gmail.com>\r\n';
        bodyText += 'Sent: \r\n';
        bodyText += 'Subject: KDZ testing... \r\n';

        test.startTest();

        EmailParser ep = new EmailParser(bodyText);

        test.stopTest();

        system.debug('---> emailInterface: ' + ep.emailInterface);

    }
}