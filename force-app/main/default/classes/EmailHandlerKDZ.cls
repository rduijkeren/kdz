/*
 * EmailHandlerKDZ
 * 
 * Modified
 * 2022-08-29 RPD
 * added code to store fromAddres and fromName on case at creation.
 * 
 */
global class EmailHandlerKDZ implements Messaging.InboundEmailHandler {

	private static final String CRLF = '\r\n';
	private static final integer MAX_BODY_LENGTH = 31000;

	static Map<String, Attachment> attachmentsMap = new Map<String, Attachment>();
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

		for (EmailServiceIgnoreEmailRule__mdt rule: [
				select DeveloperName, Label, Type__c, Value__c, Regex__c
				from EmailServiceIgnoreEmailRule__mdt
				where IsActive__c = true
			])
		{
			// System.debug('Check Ignore Rule ' + Rule.DeveloperName + ' ' + rule.Type__c + ' ' + rule.Regex__c + ' ' + Rule.Value__c);
			String value;
			if (rule.Type__c == 'Sender')
			{
				value = email.fromAddress;
			}
			else if (rule.Type__c == 'Subject')
			{
				value = email.subject;
			}
			System.debug('Value = `' + value + '\'');
			try
			{
				if ((!rule.Regex__c && value == rule.Value__c)
					|| (rule.Regex__c && Pattern.matches(rule.Value__c, value)))
				{
					// rule matches, so do nothing
					// System.debug('Rule matches, ignore email');
					return result;
				}
				// System.debug('Rule doesn\'t match');
			}
			catch (System.StringException exc)
			{
				// This can happen if the regex is invalid
				/// todo als errorlog installende
				// ErrorLog.log(
				// 	'EmailHandler',
				// 	'Error when checking ignore rule ' + rule.Label + ' (' + rule.DeveloperName + ') ' + exc.getMessage(),
				// 	exc.getStackTraceString(), exc.getLinenumber(), ErrorLog.Severity.CRITICAL, null, null
				// );
			}
		}

		System.debug('envelope'+envelope);


		contact searchContact;
		try {
			searchContact =  getContactFromEmail(email);
			//emailin.Account__c = searchContact.accountID;
		} catch (Exception e) {}



		try
		{
			boolean createCase = true; 

			Case theCase = new Case();

			Case searchCase = getCaseFromEmailThreadId(email);
			if (searchCase==null || searchCase.Id==null){
				searchCase = getCaseFromEmail(email);
			}

			if (searchCase==null || searchCase.Id==null){

				searchCase = getCaseFromEmailHeader(email);

			}

            if (searchCase.Id != null) {
				// we have identified a case. if open we update emailstatus, otherwise create a new case
				System.debug('case identified: '+searchCase.Id + ' closed: ' + searchCase.isClosed);
				// if (searchCase.isClosed==false) ****If Case = Closed then don't create a new Case****
				// {
				createCase = false;					
				update searchcase;
				
				// Store attachments
				Map<String, ContentVersion> attachmentsMap = buildAttachments(email, searchCase.Id);

				insert attachmentsMap.values();

				System.debug('saving emailmesssage on the open case: '+ searchCase.Id);
				EmailMessage newEmail = SaveEmail(createEmailMessage(email, envelope, searchCase.Id), searchCase.ContactId, searchcase.Id, attachmentsMap);

				// Store attachments
				Attachment[] attachments2insert = new Attachment[]{};
				if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
					for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
					Attachment attachment = new Attachment();
					attachment.ParentId = searchcase.Id;
					attachment.Name = email.binaryAttachments[i].filename;
					attachment.Body = email.binaryAttachments[i].body;
					attachments2insert.add( attachment );
					}
				}

				List<Attachment> attachmentList = buildAttachmentsList(newEmail, attachments2insert);

				insert attachmentList;

				// }
				if (searchCase.ContactId<>null)
					searchContact = [select id, accountID, email from Contact where id =: searchCase.ContactId limit 1];
			}


			if (createCase) 
			{
				System.debug('creating case.');

				Case c = new Case();

				KDZEmailHandler__c cs = KDZEmailHandler__c.getOrgDefaults();

				system.debug('---> cs: ' + cs);

				String caseQueueOwner = cs?.CaseOwnerName__c;

				system.debug('---> caseQueueOwner: ' + caseQueueOwner);

				ID caseQueueOwnerId = [select Id from Group where DeveloperName = :caseQueueOwner]?.Id;

				system.debug('---> caseQueueOwnerId: ' + caseQueueOwnerId);

				if (caseQueueOwnerId != null){

					c.OwnerId = caseQueueOwnerId;

				}

				system.debug('---> case.OwnerId: ' + case.OwnerId);

				c.SuppliedEmail = email.fromAddress;
				c.SuppliedName  = email.fromName;

				// c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Case').getRecordTypeId();
				c.ContactId = searchContact.Id;
				// set Account id?
				if (email.subject <> null)
					c.subject = email.subject.left(80);
				//c.Type =
				c.Origin = 'E-mail';

				if (email.htmlBody <> null)
				{
					//  System.debug ('bodylength:'+email.htmlBody.length());
					 c.Description  = (string)email.htmlBody.stripHtmlTags().left(100000);
				}
				if (email.plainTextBody <> null)
				{
					// System.debug ('plaintext nemen');
					c.Description = email.plainTextBody.left(100000); //.replaceAll('\n','<br/>');
				}

                system.debug('User:'+UserInfo.getUserId());

				insert c;
				System.debug('case inserted: '+ c.Id);
				// Store attachments
				Map<String, ContentVersion> attachmentsMap = buildAttachments(email, c.Id);

				insert attachmentsMap.values();

			//attachmentsMap = buildAttachments( email, message.id );
		 		System.debug('saving emailmesssage on the created case: '+ c.Id);
				EmailMessage newEmail = SaveEmail(createEmailMessage(email, envelope, c.Id), c.ContactId, c.Id, attachmentsMap);

				// Store attachments
				Attachment[] attachments2insert = new Attachment[]{};
				if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
					for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
					Attachment attachment = new Attachment();
					attachment.ParentId = c.Id;
					attachment.Name = email.binaryAttachments[i].filename;
					attachment.Body = email.binaryAttachments[i].body;
					attachments2insert.add( attachment );
					}
				}

				List<Attachment> attachmentList = buildAttachmentsList(newEmail, attachments2insert);

				insert attachmentList;

			}

		} catch (Exception e) {

            system.debug('---> Exception: ' + e.getMessage() + ' : ' + e.getStackTraceString());

			afp__Log__c log = afp.log_Util.createExceptionLog(e, 'EmailHandlerKDZ');
			insert log;
	// 		ErrorLog.log('EmailHandlerCustomerCare',  'Unhandled exeption. Email fromadress: ' + envelope.fromAddress + '. ' + e.getmessage(), e.getStackTraceString(), e.getLinenumber(),ErrorLog.Severity.CRITICAL, null, null);
		} 


		return result;
	}



	public static EmailMessage createEmailMessage(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope, Id caseId){
 		string crlf = '\n';

         Case theCase = [select Id, ThreadId__c from Case where Id = :caseId limit 1];

 		EmailMessage newEmail = new EmailMessage(
 				FromAddress = email.fromaddress,
				FromName = email.fromname,
				ToAddress = envelope.toaddress,
				Subject = (email.subject <> null ?  email.subject.left(80) : ''),
				HtmlBody  = (email.htmlbody <> null ?  email.htmlbody.left(MAX_BODY_LENGTH) + crlf + theCase.ThreadId__c : ''),
				TextBody  = (email.plainTextBody <> null ?  email.plainTextBody.left(MAX_BODY_LENGTH) + crlf + theCase.ThreadId__c : ''),
				Incoming = true,
				Status = '0',
				MessageDate = System.now()
			);
			
			system.debug('---> ccAddresses.size(): ' + email.ccAddresses?.size());

			if ( email.ccAddresses!=null && email.ccAddresses.size() > 0){
				string ctext = '';
				for (integer i = 0 ; i < email.ccAddresses.size() ; i++) {

					system.debug('---> email.ccAddress: ' + email.ccAddresses[i]);
					cText = cText + email.ccAddresses[i]+';';
				}
				newEmail.CcAddress = cText;
			}
			if ( email.headers!=null && email.headers.size() > 0){
				string cText = '';
				for (integer i = 0 ; i < email.headers.size() ; i++) {
					cText = cText + email.headers[i].name+' = '+email.headers[i].value+crlf;
				}
				newEmail.headers = cText;
			}

		 return newEmail;
	}


	public static EmailMessage SaveEmail(EmailMessage newEmail, id whoID, ID relatedtoId, Map<String, ContentVersion> attachmentsMap){

			if (relatedtoId <> null) {
				if (relatedtoId.getsObjectType() == Case.getsObjecttype()) {
					newemail.ParentId = relatedtoId;
					newemail.RelatedToId  = RelatedToId;
				} else {
					newemail.RelatedToId  = RelatedToId;
				}
			}

			if (WhoID <> null) {
				// door deze te zetten is de activity iig zichtbaar bij de RelatedToId (ongeacht of de mail van lead of opportunity is)
				String[] toIds = new String[]{whoID};
				newemail.toIds = toIds;
			}

            for ( String contentId : attachmentsMap.keySet() ) {
				
				String attachmentDownloadURL = url.getSalesforceBaseUrl().toExternalForm();// 'https://c.' + org.instanceName.toLowerCase() + '.content.force.com/servlet/servlet.FileDownload?file=';
				attachmentDownloadURL += '/sfc/servlet.shepherd/version/download/' + attachmentsMap.get(contentId).Id + '?asPdf=false&operationContext=CHATTER';
	
                String cid = contentId.replace( '<', '' ).replace( '>', '' );
                //String url = attachmentDownloadURL + String.valueOf( attachmentsMap.get( contentId ).id ).left( 15 );

                System.debug( 'replacing image references in html body: cid=' + cid + ', url=' + attachmentDownloadURL );

                newEmail.textBody = newEmail.textBody.replaceAll( 'cid:' + cid, attachmentDownloadURL ).left(MAX_BODY_LENGTH);
                newEmail.htmlBody = newEmail.htmlBody.replaceAll( 'cid:' + cid, attachmentDownloadURL ).left(MAX_BODY_LENGTH);

            }

			//Tag as Leveranciers-email
			//newEmail.IsLeveranciersMessage__c = true;

			system.debug('---> newEmail.htmlBody: ' + newemail.htmlBody);
			insert newemail;

			// List<ContentDocumentLink> cdlList = buildContentDocumentLinkList(attachmentsMap.values(), newEmail.Id);

			// insert cdlList;

			return newemail;
	}

	private Contact getContactFromEmail(Messaging.InboundEmail email){
		// System.debug('#### try contact ');
		Contact ctc = new Contact();
		try {
			list<Contact> lstContacts = [select id, accountID from Contact where email =: email.fromAddress limit 1];
			if (lstContacts.size() == 1){
				ctc = lstContacts[0];
				System.debug('#### OK contact found ');
			} else {
				System.debug('#### NOK contact NOT found ');
			}
		} catch (Exception e) {
			System.debug('#### Error in getContactFromEmail: ' + e.getMessage());
		}

		return ctc;
	}


	private Case getCaseFromEmail(Messaging.InboundEmail email){

		Case theCase = new Case();

		try {
			String searchBody = email.subject + CRLF + email.plainTextBody + CRLF + email.htmlBody;
			Pattern matchPattern = Pattern.compile('CAS[0-9]{8}');
			Matcher matcher = matchPattern.matcher(searchBody);
			if (matcher.find()) {
				string cnr = matcher.group(0).replace('CAS', '');
				list<Case> lstCases = [select id, contactID, accountId, isClosed from Case where CaseNumber  =: cnr limit 1];
				if (lstCases.size() == 1){
					thecase.id = lstCases[0].id;
					System.debug('#### OK Case found on (REF)number ' + cnr +  ' , isClosed: ' + thecase.isClosed );
				} else {
					System.debug('#### NOK Case NOT found on REFnumber');
				}
			}

			if (thecase.id != null) {
				thecase = [select id, OwnerID, contactID, AccountId, isClosed, ThreadId__c from Case where id =: thecase.id ];
			}

		} catch (Exception e) {
			System.debug('#### Error in getCaseFromEmail: ' + e.getMessage());
		}

        system.debug('---> getCaseFromEmail.theCase: ' + theCase);

		return theCase;
	}



	private Case getCaseFromEmailThreadId(Messaging.InboundEmail email){

		Case theCase = new Case();

		try {
			String searchBody = email.subject + CRLF + email.plainTextBody + CRLF + email.htmlBody;
			Pattern matchPattern = Pattern.compile('ref:([^:]+):ref');
			Matcher matcher = matchPattern.matcher(searchBody);
            
            //system.debug('---> searchBody: ' + searchBody);

			if (matcher.find()) {
				string emailThreadId = 'ref:'+ matcher.group(1) + ':ref';
                system.debug('---> emailThreadId: ' + emailThreadId);
				// thecase = [select id, OwnerID, contactID, AccountId, IsClosed, ThreadId__c from Case where id =: Cases.getCaseIdFromEmailThreadId(emailThreadId)];
				thecase = [select id, OwnerID, contactID, AccountId, IsClosed, ThreadId__c from Case where ThreadId__c = :emailThreadId];

            }

		} catch (Exception e) {
			System.debug('#### Error in getCaseFromEmailThreadId: ' + e.getMessage());
		}

        system.debug('---> getCaseFromEmailThreadId.theCase: ' + theCase);

		return theCase;
	}

	private Case getCaseFromEmailHeader(Messaging.InboundEmail email){

		Case theCase = new Case();

		try {

            Id caseId = Cases.getCaseIdFromEmailHeaders(email.headers);

            if (caseId <> null){
                
                thecase = [select id, OwnerID, contactID, AccountId, IsClosed, ThreadId__c from Case where id =: caseId];
                
            }
            
		} catch (Exception e) {
            System.debug('#### Error in getCaseFromEmailHeader: ' + e.getMessage());
		}
        
        system.debug('---> getCaseFromEmailHeader.theCase: ' + theCase);

		return theCase;
	}



	// private static boolean hasSObjectField(SObject so, String fieldName){
	// 	System.debug(so.getSobjectType().getDescribe().fields.getMap().keySet());
	//   	return so.getSobjectType().getDescribe().fields.getMap().keySet().contains(fieldName);
	// }

    public static Map<String, ContentVersion> buildAttachments( Messaging.InboundEmail email, ID parentId ) {

        Map<String, ContentVersion> attachmentsMap = new Map<String, ContentVersion>();

        // if attachment does not have Content-ID header
        // then we'll use this to generate a unique map key instead
        Integer noHeaderCount = 0;

        if ( email.binaryAttachments != null ) {

            for ( Messaging.InboundEmail.BinaryAttachment binaryAttachment : email.binaryAttachments ) {

                String contentId = getHeaderValue( binaryAttachment.headers, 'Content-ID' );

                if ( String.isBlank( contentId ) ) {
                    contentId = 'no-content-id-header-' + noHeaderCount++;
                }

					ContentVersion attachment = new ContentVersion();
					attachment.FirstPublishLocationId = parentId;
					attachment.PathOnClient = binaryAttachment.filename;
					attachment.Title = binaryAttachment.filename;
					attachment.VersionData = binaryAttachment.body;
					//attachment.FileType = binaryAttachments.mimeTypeSubType;
					//attachments2insert.add( attachment );

					attachmentsMap.put( contentId, attachment);

            }

        }

        if ( email.textAttachments != null ) {

            for ( Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments ) {

                String contentId = getHeaderValue( textAttachment.headers, 'Content-ID' );

                if ( String.isBlank( contentId ) ) {
                    contentId = 'no-content-id-header-' + noHeaderCount++;
                }

				ContentVersion attachment = new ContentVersion();
				attachment.FirstPublishLocationId = parentId;
				attachment.PathOnClient = textAttachment.filename;
				attachment.Title = textAttachment.filename;
				attachment.VersionData = Blob.valueOf(textAttachment.body);
				//attachment.FileType = textAttachment.mimeTypeSubType;
				//attachments2insert.add( attachment );

				attachmentsMap.put( contentId, attachment);

            }

        }

        return attachmentsMap;
    }

	private static String getHeaderValue( List<Messaging.InboundEmail.Header> headers, String name ) {

        String value = null;

        if ( headers != null ) {
            for ( Messaging.InboundEmail.Header header : headers ) {
                if ( header.name == name ) {
                    value = header.value;
                    break;
                }
            }
        }

        return value;
    }

	// private static List<ContentDocumentLink> buildContentDocumentLinkList(List<ContentVersion> cvList, ID parentId){

	// 	List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();

	// 	for (ContentVersion cv : [select Id, ContentDocumentId from ContentVersion where Id in :cvList]){

	// 		ContentDocumentLink cdl = new ContentDocumentLink();

	// 		cdl.ContentDocumentId = cv.ContentDocumentId;
	// 		cdl.LinkedEntityId = parentId;
	// 		cdl.ShareType = 'V';
	// 		cdl.Visibility = 'AllUsers';

	// 		cdlList.add(cdl);

	// 	}

	// 	return cdlList;
	// }

	private static List<Attachment> buildAttachmentsList(EmailMessage email, List<Attachment> attachments){

		List<Attachment> attachmentsList = new List<Attachment>();

		for (Attachment a : attachments) {
			Attachment attachment = new Attachment();
			attachment.ParentId = email.Id;
			attachment.Name = a.Name;
			attachment.Body = a.body;
			attachment.IsPrivate = False;
			attachmentsList.add( attachment );
		}
	
		return attachmentsList;

	}
}