public with sharing class EmailParser {

    public EmailParserInterface emailInterface = new EmailParserInterface();

    List<String> listLines = new List<String>();
    // map from Line first Token to Listline
    Map<String, List<String>> mListLine = new Map<String, List<String>>();
    List<String> listTokens = new List<String>();
    Integer tokenNumber = 0;

    public EmailParser(String stringToParse) {

        try{

        PreProcess(stringToParse);

        ParseTokens();

        PostProcess();

        } catch(exception e){

            emailInterface.success = FALSE;
            emailInterface.message = e.getMessage() + ' TokenString: ' + listTokens[tokenNumber + 1];

        }
        
        system.debug('-- listTokens: ' + listTokens);

        system.debug('---> mListLine: ' + mListLine);

        // for (String token : listTokens){

        //     system.debug('--- Token: ' + token);

        // }
        system.debug('-- emailInterface: ' + emailInterface);
        
    }
    
    private void PreProcess(String stringToParse){

        List<String> lToken = new List<String>();

        system.debug('---> find rn: ' + stringToParse.countMatches('\r\n'));
        system.debug('---> find r: ' + stringToParse.countMatches('\r'));
        system.debug('---> find n: ' + stringToParse.countMatches('\n'));
        
        listLines = stringToParse.split('\n');
    
        system.debug('-- listLines: ' + listLines);
    
        for (String line : listLines){

            lToken = new List<String>();

            line = line.replaceAll('\r', ' ');
            line = line.replaceAll('\n', ' ');

            lToken.addAll(line.split(' '));
            listTokens.addAll(lToken);

            if (!mListLine.containsKey(lToken[0])){

                mListLine.put(lToken[0], lToken);

            }
    
        }
    

    }

    private void ParseTokens(){

        Integer listSize = listTokens.size();
        String token;
        List<String> lTokens;

        emailInterface.success = TRUE;

        for (Integer i = 0; i < listTokens.size(); i++){

            tokenNumber = i;

            switch on listTokens[i] {

                when 'From:' {

                    if (i < listSize - 2){
                        lTokens = mListLine.get('From:');
                        system.debug('---> lTokens: ' + lTokens);
                        emailInterface.fromAddress = ReformatEmail(lTokens[lTokens.size() - 1]);

                    } else {

                        emailInterface.success = FALSE;
                        break;

                    }
                }

            }

        }
    }

    private void PostProcess(){

        
    }

    private Time StringToTime(String strTime){

        String[] strTimeSplit = strTime.split(':');

        return Time.newInstance(Integer.valueOf(strTimeSplit[0]), Integer.valueOf(strTimeSplit[1]), 0, 0);
    }

    private String ReformatEmail(String email){

        String str;

        str = email.mid(1, email.length() - 2);

        return str;

    }
}