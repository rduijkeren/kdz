global class EmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {

        system.debug('---> Start EmailHandler');

        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        KDZEmail__c kdze = new KDZEmail__c();

        EmailMessage em = new EmailMessage();

        EmailMessageRelation emr = new EmailMessageRelation();

        String relatedToId = '0067Z00000KumViQAJ';

        // system.debug('email: ' + email);
        // system.debug('envelope: ' + envelope);

        try{
            
            EmailParser ep = new EmailParser(email.plainTextBody);

            List<Contact> lContact = [select Id, AccountId from Contact where Email = :ep.emailInterface.fromAddress limit 1];
    
            system.debug('---> ep: ' + ep.emailInterface);

            kdze.FromEmailAddress__c = ep.emailInterface.fromAddress;
            kdze.EmailBody__c        = email.htmlBody;
            kdze.PlainTextBody__c    = email.plainTextBody;
            kdze.Subject__c          = email.subject;
            kdze.Opportunity__c      = relatedToId;

            if (lContact.size() > 0){

                kdze.Account__c = lContact[0].AccountId;
                kdze.Contact__c = lContact[0].Id;
                emr.RelationId  = lContact[0].Id;
            }

            insert kdze;

            em.fromAddress = ep.emailInterface.fromAddress;
            em.fromName    = email.fromName;
            em.status      = '2';
            em.toAddress   = 'ronald.van.duijkeren@redbookict.nl';
            em.subject     = email.subject;
            em.htmlBody    = email.htmlBody;
            em.textBody    = email.plainTextBody;
            em.incoming    = true;
            //em.relatedToId = kdze.Id;
            em.relatedToId = relatedToId;
            em.messageDate = DateTime.now();
            //em.replyTo     = email.replyTo;
            if (lContact.size() > 0){

                String[] toIds = new String[]{lContact[0].Id};

            }
            //em.toIds       = toIds; //new List<Id>{lContact[0].Id};

            insert em;

            list<EmailMessageRelation> lEMR = [select Id, RelationId, RelationType from EmailMessageRelation where EmailMessageId = :em.Id];

            if (lEMR[0].RelationType == 'FromAddress'){

                lEMR[0].RelationId = lContact[0].Id;
                lEMR[1].RelationId = '00509000006iP3eAAE';
                //lEMR[1].RelationId = '0037Z00001AIBKzQAP';

            } else{

                lEMR[0].RelationId = '00509000006iP3eAAE';
                //lEMR[0].RelationId = '0037Z00001AIBKzQAP';
                lEMR[1].RelationId = lContact[0].Id;

            }

            update lEMR;

            system.debug('---> lEMR: ' + lEMR);

            emr.EmailMessageId = em.Id;
            //emr.RelationId      = UserInfo.getUserId();
            emr.RelationAddress  = email.fromAddress;
            emr.RelationType    = 'FromAddress';
            
            //insert emr;
            
        } catch(Exception e){

            system.debug('EmailHandler Exception: ' + e.getMessage() + ' / ' + e.getStackTraceString());
            
        }

        system.debug('---> End EmailHandler');

        return result;
    }
}