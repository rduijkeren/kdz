({
    searchRecords : function(component, event, searchKey, defaultRecordId) {
        var action = component.get("c.fetchLookupValues");
        action.setParams({
            'searchKeyword': searchKey,
            'objectName' : component.get("v.objectAPIName"),
            'excludedRecordsStr' : JSON.stringify(component.get("v.listSelectedRecords")),
            'defaultRecordId' : defaultRecordId,
            'recordLabel' : component.get("v.recordLabel")
        });
        
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result && result.length == 0){
                    component.set("v.message", 'No records found...');
                } 
                else{
                    component.set("v.message", '');
                }
                component.set("v.listSearchedRecords", result); 
                
                
                if(defaultRecordId && result && result.length > 0){	 
                    var listSelectedItems =  component.get("v.listSelectedRecords"); 
                    
                    listSelectedItems.push(result[0]);
                    component.set("v.listSelectedRecords", listSelectedItems);
                    
                    var selectedIds = [];
                    listSelectedItems.forEach(function(element) {
                        selectedIds.push(element.Id);
                    });
                    component.set("v.selectedIds", selectedIds);
                    
                    var forclose = component.find("lookup-pill");
                    $A.util.addClass(forclose, 'slds-show');
                    $A.util.removeClass(forclose, 'slds-hide');
                    
                    var forclose = component.find("searchRes");
                    $A.util.addClass(forclose, 'slds-is-close');
                    $A.util.removeClass(forclose, 'slds-is-open'); 
                }
            }
        });
        
        $A.enqueueAction(action);
    },
})