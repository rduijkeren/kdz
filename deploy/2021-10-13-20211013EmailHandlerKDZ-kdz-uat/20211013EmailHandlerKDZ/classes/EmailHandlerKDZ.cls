global class EmailHandlerKDZ implements Messaging.InboundEmailHandler {

	private static final String CRLF = '\r\n';
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

		for (EmailServiceIgnoreEmailRule__mdt rule: [
				select DeveloperName, Label, Type__c, Value__c, Regex__c
				from EmailServiceIgnoreEmailRule__mdt
				where IsActive__c = true
			])
		{
			// System.debug('Check Ignore Rule ' + Rule.DeveloperName + ' ' + rule.Type__c + ' ' + rule.Regex__c + ' ' + Rule.Value__c);
			String value;
			if (rule.Type__c == 'Sender')
			{
				value = email.fromAddress;
			}
			else if (rule.Type__c == 'Subject')
			{
				value = email.subject;
			}
			System.debug('Value = `' + value + '\'');
			try
			{
				if ((!rule.Regex__c && value == rule.Value__c)
					|| (rule.Regex__c && Pattern.matches(rule.Value__c, value)))
				{
					// rule matches, so do nothing
					// System.debug('Rule matches, ignore email');
					return result;
				}
				// System.debug('Rule doesn\'t match');
			}
			catch (System.StringException exc)
			{
				// This can happen if the regex is invalid
				/// todo als errorlog installende
				// ErrorLog.log(
				// 	'EmailHandler',
				// 	'Error when checking ignore rule ' + rule.Label + ' (' + rule.DeveloperName + ') ' + exc.getMessage(),
				// 	exc.getStackTraceString(), exc.getLinenumber(), ErrorLog.Severity.CRITICAL, null, null
				// );
			}
		}

		System.debug('envelope'+envelope);


		contact searchContact;
		try {
			searchContact =  getContactFromEmail(email);
			//emailin.Account__c = searchContact.accountID;
		} catch (Exception e) {}



		try
		{
			boolean createCase = true; 

			Case theCase = new Case();

			Case searchCase = getCaseFromEmailThreadId(email);
			if (searchCase==null || searchCase.Id==null){
				searchCase = getCaseFromEmail(email);
			}

			if (searchCase==null || searchCase.Id==null){

				searchCase = getCaseFromEmailHeader(email);

			}

            if (searchCase.Id != null) {
				// we have identified a case. if open we update emailstatus, otherwise create a new case
				System.debug('case identified: '+searchCase.Id + ' closed: ' + searchCase.isClosed);
				// if (searchCase.isClosed==false) ****If Case = Closed then don't create a new Case****
				// {
					createCase = false;					
					update searchcase;
					
					// Store attachments
					Attachment[] attachments2insert = new Attachment[]{};
					if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
					  for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
						Attachment attachment = new Attachment();
						attachment.ParentId = searchcase.Id;
						attachment.Name = email.binaryAttachments[i].filename;
						attachment.Body = email.binaryAttachments[i].body;
						attachments2insert.add( attachment );
					  }
					}
					insert attachments2insert;
					System.debug('saving emailmesssage on the open case: '+ searchCase.Id);
					SaveEmail(createEmailMessage(email, envelope, searchCase.Id), searchCase.ContactId, searchcase.Id, attachments2insert);

				// }
				if (searchCase.ContactId<>null)
					searchContact = [select id, accountID, email from Contact where id =: searchCase.ContactId limit 1];
			}


			if (createCase) 
			{
				System.debug('creating case.');

				Case c = new Case();
				// c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Case').getRecordTypeId();
				c.ContactId = searchContact.Id;
				// set Account id?
				if (email.subject <> null)
					c.subject = email.subject.left(80);
				//c.Type =
				c.Origin = 'E-mail';

				if (email.htmlBody <> null)
				{
					//  System.debug ('bodylength:'+email.htmlBody.length());
					 c.Description  = (string)email.htmlBody.stripHtmlTags().left(100000);
				}
				if (email.plainTextBody <> null)
				{
					// System.debug ('plaintext nemen');
					c.Description = email.plainTextBody.left(100000); //.replaceAll('\n','<br/>');
				}

                system.debug('User:'+UserInfo.getUserId());

				insert c;
				System.debug('case inserted: '+ c.Id);
				// Store attachments
				Attachment[] attachments2insert = new Attachment[]{};
				if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
				  for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
					Attachment attachment = new Attachment();
					attachment.ParentId = c.Id;
					attachment.Name = email.binaryAttachments[i].filename;
					attachment.Body = email.binaryAttachments[i].body;
					attachments2insert.add( attachment );
				  }
				}
				insert attachments2insert;
		 		System.debug('saving emailmesssage on the created case: '+ c.Id);
				SaveEmail(createEmailMessage(email, envelope, c.Id), c.ContactId, c.Id, attachments2insert);

			}

		} catch (Exception e) {
            system.debug('---> Exception: ' + e.getMessage() + ' : ' + e.getStackTraceString());
	// 		ErrorLog.log('EmailHandlerCustomerCare',  'Unhandled exeption. Email fromadress: ' + envelope.fromAddress + '. ' + e.getmessage(), e.getStackTraceString(), e.getLinenumber(),ErrorLog.Severity.CRITICAL, null, null);
		} 


		return result;
	}



	public static EmailMessage createEmailMessage(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope, Id caseId){
 		string crlf = '\n';

         Case theCase = [select Id, ThreadId__c from Case where Id = :caseId limit 1];

 		EmailMessage newEmail = new EmailMessage(
 				FromAddress = email.fromaddress,
				FromName = email.fromname,
				ToAddress = envelope.toaddress,
				Subject = (email.subject <> null ?  email.subject.left(80) : ''),
				HtmlBody  = (email.htmlbody <> null ?  email.htmlbody.left(30000) + crlf + theCase.ThreadId__c : ''),
				TextBody  = (email.plainTextBody <> null ?  email.plainTextBody.left(30000) + crlf + theCase.ThreadId__c : ''),
				Incoming = true,
				Status = '0',
				MessageDate = System.now()
				);
			if ( email.ccAddresses!=null && email.ccAddresses.size() > 0){
				string ctext = '';
				for (integer i = 0 ; i < email.ccAddresses.size() ; i++) {
					cText = cText + email.ccAddresses[i]+';';
				}
				newEmail.CcAddress = cText;
			}
			if ( email.headers!=null && email.headers.size() > 0){
				string cText = '';
				for (integer i = 0 ; i < email.headers.size() ; i++) {
					cText = cText + email.headers[i].name+' = '+email.headers[i].value+crlf;
				}
				newEmail.headers = cText;
			}

		 return newEmail;
	}


	public static EmailMessage SaveEmail(EmailMessage newEmail, id whoID, ID relatedtoId, Attachment[] attachments){

			if (relatedtoId <> null) {
				if (relatedtoId.getsObjectType() == Case.getsObjecttype()) {
					newemail.ParentId = relatedtoId;
					newemail.RelatedToId  = RelatedToId;
				} else {
					newemail.RelatedToId  = RelatedToId;
				}
			}

			if (WhoID <> null) {
				// door deze te zetten is de activity iig zichtbaar bij de RelatedToId (ongeacht of de mail van lead of opportunity is)
				String[] toIds = new String[]{whoID};
				newemail.toIds = toIds;
			}

			//Tag as Leveranciers-email
			//newEmail.IsLeveranciersMessage__c = true;

			insert newemail;
			// System.debug('EMR:'+[select id, relationId, relationType from EmailMessageRelation where emailMessageId = :newemail.Id]);


			// try {
			// 	if (WhoID <> null) {
			// 		// skip for case becasue parentid is set and then emr is done by SF
			// 		if (newemail.ParentId == null && WhoId.getsObjectType() == Contact.getsObjecttype()) {
			// 			// Add Email Message Relation for id of the sender
			// 			EmailMessageRelation emr = new EmailMessageRelation();
			// 			emr.emailMessageId = newemail.id;
			// 			emr.relationId = whoID;
			// 			emr.relationType = 'FromAddress';
			// 			insert emr;
			// 		}
			// 	}
			// } catch (Exception e) {
			// 	System.debug( 'Error Saving emailrelation: '+ e.getmessage() );
			// 	///ErrorLog.log('EmailHandlerCustomerCare',  'Error Saving emailrelation: '+ e.getmessage() + '\n WhoId' + whoId + ' relatedtoId:' + relatedtoId, e.getStackTraceString(), e.getLinenumber(),ErrorLog.Severity.ERROR,  newemail.id, whoID);
			// }


			Attachment[] attachments2insert = new Attachment[]{};
			for (Attachment a : attachments) {
				Attachment attachment = new Attachment();
				attachment.ParentId = newemail.Id;
				attachment.Name = a.Name;
				attachment.Body = a.body;
				attachments2insert.add( attachment );
			  }
			if (!attachments2insert.isEmpty())
				insert attachments2insert;

			return newemail;
	}



	private Contact getContactFromEmail(Messaging.InboundEmail email){
		// System.debug('#### try contact ');
		Contact ctc = new Contact();
		try {
			list<Contact> lstContacts = [select id, accountID from Contact where email =: email.fromAddress limit 1];
			if (lstContacts.size() == 1){
				ctc = lstContacts[0];
				System.debug('#### OK contact found ');
			} else {
				System.debug('#### NOK contact NOT found ');
			}
		} catch (Exception e) {
			System.debug('#### Error in getContactFromEmail: ' + e.getMessage());
		}

		return ctc;
	}


	private Case getCaseFromEmail(Messaging.InboundEmail email){

		Case theCase = new Case();

		try {
			String searchBody = email.subject + CRLF + email.plainTextBody + CRLF + email.htmlBody;
			Pattern matchPattern = Pattern.compile('CAS[0-9]{8}');
			Matcher matcher = matchPattern.matcher(searchBody);
			if (matcher.find()) {
				string cnr = matcher.group(0).replace('CAS', '');
				list<Case> lstCases = [select id, contactID, accountId, isClosed from Case where CaseNumber  =: cnr limit 1];
				if (lstCases.size() == 1){
					thecase.id = lstCases[0].id;
					System.debug('#### OK Case found on (REF)number ' + cnr +  ' , isClosed: ' + thecase.isClosed );
				} else {
					System.debug('#### NOK Case NOT found on REFnumber');
				}
			}

			if (thecase.id != null) {
				thecase = [select id, OwnerID, contactID, AccountId, isClosed, ThreadId__c from Case where id =: thecase.id ];
			}

		} catch (Exception e) {
			System.debug('#### Error in getCaseFromEmail: ' + e.getMessage());
		}

        system.debug('---> getCaseFromEmail.theCase: ' + theCase);

		return theCase;
	}



	private Case getCaseFromEmailThreadId(Messaging.InboundEmail email){

		Case theCase = new Case();

		try {
			String searchBody = email.subject + CRLF + email.plainTextBody + CRLF + email.htmlBody;
			Pattern matchPattern = Pattern.compile('ref:([^:]+):ref');
			Matcher matcher = matchPattern.matcher(searchBody);
            
            //system.debug('---> searchBody: ' + searchBody);

			if (matcher.find()) {
				string emailThreadId = 'ref:'+ matcher.group(1) + ':ref';
                system.debug('---> emailThreadId: ' + emailThreadId);
				// thecase = [select id, OwnerID, contactID, AccountId, IsClosed, ThreadId__c from Case where id =: Cases.getCaseIdFromEmailThreadId(emailThreadId)];
				thecase = [select id, OwnerID, contactID, AccountId, IsClosed, ThreadId__c from Case where ThreadId__c = :emailThreadId];

            }

		} catch (Exception e) {
			System.debug('#### Error in getCaseFromEmailThreadId: ' + e.getMessage());
		}

        system.debug('---> getCaseFromEmailThreadId.theCase: ' + theCase);

		return theCase;
	}

	private Case getCaseFromEmailHeader(Messaging.InboundEmail email){

		Case theCase = new Case();

		try {

            Id caseId = Cases.getCaseIdFromEmailHeaders(email.headers);

            if (caseId <> null){
                
                thecase = [select id, OwnerID, contactID, AccountId, IsClosed, ThreadId__c from Case where id =: caseId];
                
            }
            
		} catch (Exception e) {
            System.debug('#### Error in getCaseFromEmailHeader: ' + e.getMessage());
		}
        
        system.debug('---> getCaseFromEmailHeader.theCase: ' + theCase);

		return theCase;
	}



	// private static boolean hasSObjectField(SObject so, String fieldName){
	// 	System.debug(so.getSobjectType().getDescribe().fields.getMap().keySet());
	//   	return so.getSobjectType().getDescribe().fields.getMap().keySet().contains(fieldName);
	// }



}