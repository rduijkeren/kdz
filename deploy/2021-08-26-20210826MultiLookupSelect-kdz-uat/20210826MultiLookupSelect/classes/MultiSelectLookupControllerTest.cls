/*
    @Purpose    : Test class of MultiSelectLookupController 
*/

@isTest
private class MultiSelectLookupControllerTest {
	// static testMethod void validateSearchFunction() {
	// 	List<Account> accountList = new List<Account>();
	// 	List<MultiSelectLookupController.RecordWrapper> excludedRecords = new List<MultiSelectLookupController.RecordWrapper>();
	// 	List<MultiSelectLookupController.RecordWrapper> searchedAccountList = new List<MultiSelectLookupController.RecordWrapper>();

	// 	for (Integer count = 0; count < 200; count++) {
	// 		accountList.add(new Account(Name = 'Test account' + count));
	// 	}

	// 	insert accountList;

	// 	Test.startTest();
	// 	excludedRecords = MultiSelectLookupController.fetchLookupValues(
	// 		'',
	// 		'Account',
	// 		JSON.serialize(new List<MultiSelectLookupController.RecordWrapper>()),
	// 		'',
	// 		'Name'
	// 	);
	// 	searchedAccountList = MultiSelectLookupController.fetchLookupValues(
	// 		'Test',
	// 		'Account',
	// 		JSON.serialize(excludedRecords),
	// 		'',
	// 		'Name'
	// 	);

	// 	Test.stopTest();

	// 	System.assert(searchedAccountList.size() == 5);

	// 	searchedAccountList = MultiSelectLookupController.fetchLookupValues(
	// 		'Test',
	// 		'User',
	// 		JSON.serialize(new List<MultiSelectLookupController.RecordWrapper>()),
	// 		'',
	// 		'UserName'
	// 	);
	// }
	static testMethod void validateSearchFunctionContact() {
		List<Contact> contactList = new List<Contact>();
		List<MultiSelectLookupController.RecordWrapper> excludedRecords = new List<MultiSelectLookupController.RecordWrapper>();
		List<MultiSelectLookupController.RecordWrapper> searchedContactList = new List<MultiSelectLookupController.RecordWrapper>();

		for (Integer count = 0; count < 200; count++) {
			contactList.add(new Contact(LastName = 'Test contact' + count));
		}

		insert contactList;

		Test.startTest();
		excludedRecords = MultiSelectLookupController.fetchLookupValues(
			'',
			'Contact',
			JSON.serialize(new List<MultiSelectLookupController.RecordWrapper>()),
			'',
			'Name'
		);
		searchedContactList = MultiSelectLookupController.fetchLookupValues(
			'Test',
			'Contact',
			JSON.serialize(excludedRecords),
			'',
			'Name'
		);

		Test.stopTest();

		System.assert(searchedContactList.size() == 5);

		searchedContactList = MultiSelectLookupController.fetchLookupValues(
			'Test',
			'User',
			JSON.serialize(new List<MultiSelectLookupController.RecordWrapper>()),
			'',
			'UserName'
		);
	}
}