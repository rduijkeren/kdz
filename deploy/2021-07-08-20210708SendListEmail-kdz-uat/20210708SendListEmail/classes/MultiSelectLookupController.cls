/*
    @Purpose    : Controller class of MultiSelectLookup lightning component
*/

public with sharing class MultiSelectLookupController {
	/*
        @Purpose    : Fetch the object records and pass it to the component dynamically
    */
	@AuraEnabled
	public static List<RecordWrapper> fetchLookupValues(
		String searchKeyword,
		String objectName,
		String excludedRecordsStr,
		String defaultRecordId,
		String recordLabel
	) {
		List<RecordWrapper> resultList = new List<RecordWrapper>();
		List<RecordWrapper> excludedRecords = new List<RecordWrapper>();
		String searchKey = '%' + searchKeyword + '%';

		excludedRecords = (List<RecordWrapper>) JSON.deserialize(excludedRecordsStr, List<RecordWrapper>.class);

		List<String> excludedRecordIds = new List<string>();
		for (RecordWrapper record : excludedRecords) {
			excludedRecordIds.add(record.Id);
		}

		// Fetch Record List with LIMIT 5 and exclude already selected records
		String query = 'SELECT Id, Name ';

		if (String.isNotBlank(recordLabel) && !recordLabel.equalsIgnoreCase('Name')) {
			query += ', ' + recordLabel;
		}

		if (String.isNotBlank(recordLabel) && !recordLabel.equalsIgnoreCase('Name')) {
			query +=
				' FROM ' +
				objectName +
				' WHERE (Name LIKE: searchKey OR ' +
				recordLabel +
				' LIKE: searchKey )' +
				' AND Id NOT IN : excludedRecordIds ';
		} else {
			query += ' FROM ' + objectName + ' WHERE Name LIKE: searchKey AND Id NOT IN : excludedRecordIds ';
		}

		// Search for default record
		if (String.isNotBlank(defaultRecordId)) {
			query += ' AND Id =: defaultRecordId';
		}

		// Check for only active users if object to be queried is user
		if (String.isNotBlank(objectName) && objectName.equalsIgnoreCase('User')) {
			query += ' AND IsActive = true ';
		}

		query += ' ORDER BY CreatedDate DESC LIMIT 5';

		List<sObject> lstOfRecords = Database.query(query);

		for (sObject record : lstOfRecords) {
			RecordWrapper wrapper = new RecordWrapper();
			String label = (String) record.get(recordLabel);

			if (String.isNotBlank(label) && !recordLabel.equalsIgnoreCase('Name')) {
				wrapper.Subtitle = label;
			} else {
				wrapper.Subtitle = '';
			}

			wrapper.Name = (String) record.get('Name');
			wrapper.Id = (String) record.get('Id');
			resultList.add(wrapper);
		}

		return resultList;
	}

	public class RecordWrapper {
		@AuraEnabled
		public String Id;
		@AuraEnabled
		public String Name;
		@AuraEnabled
		public String Subtitle;
	}
}