({
    selectRecord : function(component, event, helper){      
        // Get the selected record from list  
        var selectedRecord = component.get("v.oRecord"); 
        
        // Call the event   
        var event = component.getEvent("oSelectedRecordEvent");
        
        // Set the selected sObject record to the event attribute.  
        event.setParams({"recordByEvent" : selectedRecord });  
        
        // Fire the event  
        event.fire();
    },
})