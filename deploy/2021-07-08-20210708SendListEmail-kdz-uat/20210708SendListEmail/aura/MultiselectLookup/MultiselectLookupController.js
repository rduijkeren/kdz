({
    // Initiatlise if there's any defaultRecordId passed to the component
    // This will prepopulate a default record in the pills
    doInit : function(component, event, helper){     
    	var defaultRecordId = component.get("v.defaultRecordId");
        
        if(defaultRecordId){
            helper.searchRecords(component, event, '', defaultRecordId); 
        }
	},
    
    // On mouse leave clear the listSearchedRecords & hide the search result component 
    onblur : function(component, event, helper){        
        component.set("v.listSearchedRecords", null );
        component.set("v.searchKeyword", '');
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    // Show the spinner, show child search result component and call helper function
    onfocus : function(component, event, helper){
        
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        component.set("v.listSearchedRecords", null ); 
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        
        // Get default 5 records ORDER BY CreatedDate DESC 
        var searchKey = '';
        helper.searchRecords(component, event, searchKey, '');
    },
    
    keyPressController : function(component, event, helper) {
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        
        // Get the search keyword   
        var searchKey = component.get("v.searchKeyword");
        
        // Check if searchKey length is more then 0 then open the lookup result list and call the helper 
        // else close the lookup result list section.   
        if(searchKey.length > 0){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchRecords(component, event, searchKey, '');
        }
        else{  
            component.set("v.listSearchedRecords", null ); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    
    // Clear record selection
    clear :function(component, event, heplper){        
        console.log('event.target.value;',event.target.name);
        var selectedPillId = event.target.name;
        var allPillsList = component.get("v.listSelectedRecords"); 
        
        if(allPillsList && allPillsList.length <= 1){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning!",
                "message": "At least one record should be selected!",
                "type" : "warning"
            });
            toastEvent.fire();
        }
        else{            
            for(var i = 0; i < allPillsList.length; i++){
                if(allPillsList[i].Id == selectedPillId){
                    allPillsList.splice(i, 1);
                    component.set("v.listSelectedRecords", allPillsList);
                    
                    var selectedIds = [];
                    allPillsList.forEach(function(element) {
                        selectedIds.push(element.Id);
                    });
                    component.set("v.selectedIds", selectedIds);
                }  
            }
            component.set("v.searchKeyword", null);
            component.set("v.listSearchedRecords", null);
        }              
    },
    
    // This function is called when the user selects any record from the result list.   
    handleComponentEvent : function(component, event, helper) {        
        component.set("v.searchKeyword", null);
        
        // Get the selected object record from the COMPONENT event 	 
        var listSelectedItems =  component.get("v.listSelectedRecords");
        var recordFromEvent = event.getParam("recordByEvent");
		
        listSelectedItems.push(JSON.parse(JSON.stringify(recordFromEvent)));
        component.set("v.listSelectedRecords", listSelectedItems);
        
        var selectedIds = [];
        listSelectedItems.forEach(function(element) {
            selectedIds.push(element.Id);
        });
        component.set("v.selectedIds", selectedIds);
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open'); 
    },
})