@isTest
public with sharing class EmailHandlerKDZTest {

    @TestSetup
    static void makeData(){
        Account testAccount = new Account(
            Name = 'Test Account',
            Type = 'Customer',
            Website = 'www.salesforce.com',
            Industry = 'Other'
        );

        insert testAccount;

        Contact testContact = new Contact(
            LastName = 'Test',
            Phone = '123',
            AccountId = testAccount.Id,
            Email = 'test@example.com'
        );

        insert testContact;

        Case testCase = new Case(
            Type = 'Problem',
            Origin = 'Email',
            Subject = 'Test Case',
            AccountId = testAccount.Id,
            ContactId = testContact.Id
        );

        insert testCase;  
    
    }

    public static testMethod void emailTestWithCase() {

        Case c = [select id, casenumber from case limit 1];

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        email.subject = 'Test Subject';
        email.plainTextBody = 'Hello, this a test email for case ';


        env.fromAddress = 'test@example.com';
        email.ccAddresses = new List<String>{'mail@mail.com'};

        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

        EmailHandlerKDZ emailHandler = new EmailHandlerKDZ();
        
        Messaging.InboundEmailResult result = emailHandler.handleInboundEmail(email, env);

        System.assertEquals( result.success  ,true);

    }    

    public static testMethod void emailTestWithCaseWithRef() {

        Case c = [select id, casenumber,ThreadId__c from case limit 1];

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        email.subject = 'Test Subject';
        email.plainTextBody = 'ref:' + c.ThreadId__c + ':ref' +  ' Hello, this a test email for case ';


        env.fromAddress = 'test@example.com';

        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

        EmailHandlerKDZ emailHandler = new EmailHandlerKDZ();
        
        Messaging.InboundEmailResult result = emailHandler.handleInboundEmail(email, env);

        System.assertEquals( result.success  ,true);

    }    


    public static testMethod void emailTestWithoutCase() {

        Case c = [select id, casenumber from case limit 1];

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();

        email.subject = 'Test Subject';
        email.plainTextBody = 'Hello, this a test email for no case ';

        env.fromAddress = 'test@example.com';
        
        EmailHandlerKDZ emailHandler = new EmailHandlerKDZ();
        
        Messaging.InboundEmailResult result = emailHandler.handleInboundEmail(email, env);
        System.assertEquals( result.success  ,true);

    }    

    public class CustomMetadataService {

        public CustomMetadataService() {}
    
        /**
         * This method instantiates a custom metadata record of type MyCustomMetadataType__mdt
         * and sets the DeveloperName to the input String.
         * Note that the record is not inserted into the database, 
         * and would not be found by a SOQL query.
         */
        public EmailServiceIgnoreEmailRule__mdt getCustomMetadataRecord(String myName) {
            EmailServiceIgnoreEmailRule__mdt theRecord = new EmailServiceIgnoreEmailRule__mdt();
            theRecord.DeveloperName = myName;
            return theRecord;
        }
    
        /**
         * This method retrieves a custom metadata record, changes a field, and returns it
         * to the caller. Note that the changed record is not updated in the database.
         */
        public EmailServiceIgnoreEmailRule__mdt getChangedCustomMetadataRecord(String myNewName) {
            EmailServiceIgnoreEmailRule__mdt theRecord = [SELECT Id, DeveloperName from EmailServiceIgnoreEmailRule__mdt LIMIT 1];
            theRecord.DeveloperName = myNewName;
            

            return theRecord;
        }
    
    }
    
}